package com.example.elyes.marketplace1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button buttonlogin ;
    private EditText login , password ;
    public static  final String NAME_="name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonlogin = (Button) findViewById(R.id.buttonlogin);
        login = (EditText)findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);

        buttonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(login.getText().toString().equals("elyes") && password.getText().toString().equals("elyes")){
                 Intent intent  = new Intent(MainActivity.this,Home.class);
                    intent.putExtra(NAME_,login.getText().toString());
                    startActivity(intent);

                }else {
                    Toast.makeText(MainActivity.this,
                            "error ", Toast.LENGTH_LONG).show();
                }


            }
        });


    }
}
