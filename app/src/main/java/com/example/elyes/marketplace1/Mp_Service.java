package com.example.elyes.marketplace1;

/**
 * Created by elyes on 3/14/2018.
 */

public class Mp_Service {

    private Long id_service;
    private String service_name;
    private Boolean status;
    private Float available_storage;
    private String description;
    private Float price;
    private String get_started;
    private Byte[] image;
    private String imageURL;

    public Mp_Service(Long id_service, String service_name, Boolean status, Float available_storage, String description, Float price, String get_started, Byte[] image) {
        this.id_service = id_service;
        this.service_name = service_name;
        this.status = status;
        this.available_storage = available_storage;
        this.description = description;
        this.price = price;
        this.get_started = get_started;
        this.image = image;
    }

    public Mp_Service(String service_name, Boolean status, String imageURL) {
        this.service_name = service_name;
        this.status = status;
        this.imageURL = imageURL;
    }

    public Mp_Service() {
    }

    public Long getId_service() {
        return id_service;
    }

    public void setId_service(Long id_service) {
        this.id_service = id_service;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Float getAvailable_storage() {
        return available_storage;
    }

    public void setAvailable_storage(Float available_storage) {
        this.available_storage = available_storage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getGet_started() {
        return get_started;
    }

    public void setGet_started(String get_started) {
        this.get_started = get_started;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
