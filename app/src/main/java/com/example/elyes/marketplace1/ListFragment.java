package com.example.elyes.marketplace1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ListFragment extends Fragment {

    private RecyclerView myRecylerView ;
    private List<Mp_Service> ServiceList;
    private AdapterService adapterService;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        ServiceList =  new ArrayList<>();

        myRecylerView = (RecyclerView) v.findViewById(R.id.serviceslist);
        adapterService = new AdapterService(ServiceList,getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());
        myRecylerView.setAdapter(adapterService);

   loaddata();
        return v  ;

    }
    void loaddata(){
        ServiceList.clear();
        ServiceList.addAll(dataobjects.getAllData());
        adapterService.notifyDataSetChanged();
    }




}
