package com.example.elyes.marketplace1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by elyes on 3/14/2018.
 */

public class AdapterService extends RecyclerView.Adapter<AdapterService.MyViewHolder> {
    private List<Mp_Service> ServiceList;
    private Context context;
    public  class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date,status ;
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            date  = (TextView) view.findViewById(R.id.date);
            status  = (TextView) view.findViewById(R.id.status);
            imageView= (ImageView) view.findViewById(R.id.movieimage);




        }
    }
    public AdapterService(List<Mp_Service> ServiceList,@NonNull Context context) {
        this.context = context;
        this.ServiceList = ServiceList;


    }

    @Override
    public int getItemCount() {
        return ServiceList.size();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Mp_Service mp_service = ServiceList.get(position);
        holder.title.setText(mp_service.getService_name());
        holder.date.setText("22-01-2018");
        if (mp_service.getStatus()==true) {
            holder.status.setText("UP");
        }else {
            holder.status.setText("DOWN");

        }
        Picasso.with(context).load(mp_service.getImageURL()).placeholder(R.drawable.loader).into(holder.imageView);
        Log.d("settt",mp_service.getImageURL());




    }



}